# Naming Things

You can see the slides live at <https://mbergeron.gitlab.io/naming-things/>


## Building locally

Clone the source, then run `hugo server`, the slides should be available at <http://localhost:1313/>