+++
outputs = ["Reveal"]
title = "Naming Things"
+++

---

# Bonjour SagLac.io!


<h3 style="opacity:0.6">Micaël Bergeron</h3>

---

# {{< frag c="Naming" >}} {{< frag c="Things" >}}

### _{{< frag c="or the Art of Naming Stuff" >}}_

---

> "There are only two hard things in Computer Science: cache invalidation and naming things." -- Phil Karlton

{{% fragment %}} Mais la cache, tout le monde s'en fout. {{% /fragment %}}

---

# Parlons noms

---

## Pourquoi nommons-nous les choses?

### {{< frag c="PARCE" >}} {{< frag c="QUE" >}} {{< frag c="." >}}

---

# En programmation?

- Définir le domaine d'affaires
- Bâtir le modèle virtuel des processus
- Aide à l'architecture de l'application

---

# Et ça donne quoi?

- Code lisible et intuitif
- Association précise de plusieurs domaines
- Compréhension contextuelle et segmentée
- Collaboration plus facile

---

{{< slide class="color-inverse" 
    background-color="black" 
    background-opacity="0.7"
    background-image="img/arbre-sm-cmp.jpg" >}}

### Définir le domaine

{{< frag c="Plante" >}}  
{{< frag c="Arbre" >}}  
{{< frag c="Bouleau Jaune" >}}  
{{< frag c="Betula alleghaniensis" >}}

<a class="credit-badge" href="https://unsplash.com/@johnprice?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from John Price"><span style="display:inline-block;padding:2px 3px"></span><span style="display:inline-block;padding:2px 3px">Photo by John Price</span></a>

---

{{< slide class="color-inverse" 
    background-image="img/coffea-cmp.jpg" 
    background-color="black" 
    background-opacity="0.7" >}}

### Définir le domaine

{{< frag c="Rubiaceae" >}}  
{{< frag c="Coffea" >}}  
{{< frag c="Caffea arabica" >}}  

---

{{< slide class="has-text-shadow" 
    background-opacity="0.2"
    background-size="contain"
    background-image="img/caffeine-cmp.png" >}}

### Définir le domaine

{{< frag c="Caféine" >}}  
{{< frag c="1,3,7-triméthylxanthine" >}}  
{{< frag c="méthylthéobromine" >}}  
{{< frag c="méthylthéophylline" >}}  
{{< frag c="E970" >}}  

---

## Maître de son Domaine

Chaque mots fait référence à un domaine.

{{% fragment %}} Il est important de s'assurer de la **cohérence** globale. {{% /fragment %}}

---

### Bâtir le modèle virtuel

- Choisir les éléments du domaine d'affaires
- Exprimer les processus d'affaires à l'aide des ces éléments
- Encapsuler l'implémentation des processus

{{% note %}}

- Biotech
- Fintech
- Data Science

{{% /note %}}

---

### Exemples!

`x`, `y`, `z` peuvent représenter les axes dans un environnement 3D.

<code class="highlight">
  {{< frag c="w" >}}
  {{< frag c="basis" >}}
  {{< frag c="vector" >}}
  {{< frag c="cross" >}}
</code>

{{% fragment %}} Font aussi partie du même domaine. {{% /fragment %}}

---

### Exemples!

`payload` peut faire référence à protocole de messages.

<code class="highlight">
  {{< frag c="message" >}}
  {{< frag c="envelope" >}}
  {{< frag c="header" >}}
  {{< frag c="request" >}}
</code>

{{% fragment %}} Font aussi partie du même domaine. {{% /fragment %}}

---

### Exemples!

`authentification` peut faire référence à un contrôle d'accès.

<code class="highlight">
  {{< frag c="identification" >}}
  {{< frag c="authorization" >}}
  {{< frag c="access control" >}}
  {{< frag c="credentials" >}}
</code>

{{% fragment %}} Font aussi partie du même domaine. {{% /fragment %}}

---

# "Semantic Naming"

### L'approche

- Le Nom Sémantique
- Le Contexte d'Invocation
- Le Principe de Réduction
- La Qualification

---

## Nom Sémantique

Il s'agit de la combinaison d'une **action**, d'un **sujet** et d'un **produit**.

---

### La Forme Canonique

> Du grec (κανονικός "kanonikos", "relatif aux règles" - κανόνες, "kanones", "les règles") et du latin canonicus (« conforme aux règles, au canon ; régulier »).

Lors de la création d'un nouveau composant, il est important de définir sa forme canonique.

---

Il s'agit de la base de l'identifiant.

```python
class Author:
    """
    Canonical name: author
    """
    pass
```

```python
class ConnectionFactory:
    """
    Canonical name: connection_factory
    """
    pass
```

```python
class UDPConnection:
    """
    Canonical name: udp_connection
    """
    pass
```

---

### Le cas des primitives

_Les primitives n'ont pas de forme canonique._

On peut toutefois nommer des primitives de manière sémantique en exprimant ce que **représente leurs contenu**

> id count length size width index weight etc…

---

*Il est primordial de faire des abstractions.*

---

### Nom Sémantique — Les actions

Les actions sont représentées par un verbe. 

> get is fetch load delete lock acquire pull destroy find update set has can, etc… 

{{% fragment %}} _La présence d'une action implique que l'identifiant représente une fonction._ {{% /fragment %}}

---

*Un conseil pour les actions, faites un lexique.*

```
is:      implique qu'une fonction doit retourner bool
try:     implique qu'une fonction peut retourner None
search:  trouve tous les résultats possibles
find:    trouve un seul résultat ou `throw` une exception
get:     trouve l'élément selon une clé naturelle (id, index)
load:    charge et décode une valeur
fetch:   fait une requête externe et décode une valeur
destroy: supprime toutes les représentations stocké d'une valeur
delete:  marque une valeur comme étant supprimée
…
```

---

### Nom sémantique — Le sujet

Le sujet est représenté par la **forme canonique** d'un composant.

> user account handler connection service page transaction payment card, etc…

{{% fragment %}} _Le sujet doit correspondre à un argument de la fonction._  {{% /fragment %}}

---

Dans le cas d'une méthode, le sujet est dit _implicite_ et correspond à l'instance de la méthode.

---

### Nom sémantique — Le produit

Le produit est représenté par la **forme canonique** d'un composant et doit correspondre à la valeur finale de l'identifiant.

---

## Le Contexte d'invocation

Le contexte d'invocation est composé des entités qui peuvent être inférées au site de l'invocation.

Il dépend de la structure du code et/ou des conventions de noms établie.

> class modules if for while, etc…

{{% fragment %}} **Il sera utilisé pour réduire les identifiants présents à l'intérieur du contexte.** {{% /fragment %}}

---

<section data-noprocess>
    controller/user.py

    <pre class="stretch"><code data-trim data-noescape>
# contexte: user
	
class UserController(Controller):
    """
    Canonical form: user_controller
    """
    
    # contexte: user_controller
    def create(self, user: User):
        …
        
    # contexte: user_controller
    def search(self, params: dict):
        …
		
	# contexte: user_controller
	def delete(self, user: User):
	    …
    </code></pre>
</section>

---

## Le Principe de Réduction

On peut faire l'élision du **sujet** ou du **produit** lorsque le contexte le permet.

```python
def create_user(attrs={}) -> User:
	…
```

```python
# contexte: user
def create(attrs={}) -> User:
	…
```

```python
# contexte: user
def delete(user: User):
	…
```

---

{{< slide class="color-inverse" background-image="img/code-cmp.jpg" >}}

# CODE ALERT!

---

<section data-noprocess>
    <pre class="stretch"><code data-trim data-noescape>
class Payment:
    # action: build
    # sujet: — (primitive)
    # produit: payment (réduit)
    @classmethod
    def build(cls, attrs={}) -> 'Payment':
        …

    # action: delete
	# sujet: payment (implicite)
    def delete(self):
        …
        
    # action: delete
    # sujet: payments
    @classmethod
    def delete_payments(cls, payments: Iterable['Payment']) -> int:
        …

    # action: create
    # suject: payment (implicite)
    def create(self):
        …

        </code></pre>
</section>

---

On peut se rendre compte qu'une fonction n'est pas à sa place lorsque le principe de réduction donne un résultat incohérent.

```python
class Payment:
    # contexte: payment
    # action: —
    # sujet: transaction
    # produit: payments
    @classmethod
    def transaction_payments(cls, transaction: …) -> …:
        …
```

```python
class Transaction:
    # sujet: — (transaction)
    # produit: payments
    @property
    def payments(self) -> Iterable['Payment']:
        …
```

---

<section data-noprocess>
    <pre class="stretch"><code data-trim data-noescape>
class Connection:
    "Canonical form: connection"
    pass
    
    
class ConnectionManager(…):
    """
    Canonical form: connection_manager
    """

    def __init__(self, connection_hander: ConnectionHandler):
        self.handler = connection_handler
        
    # action: open
    # produit: connection (réduit)
    def open(self, uri: str) -> Connection:
        …
        
    # action: close
    # sujet: connection (réduit)
    def close(self, connection: Connection):
        …
        </code></pre>
</section>

---

## La Qualification

Lorsqu'un contexte inclut plusieurs identifiants identiques, il faut alors les qualifier.

---

```python
@route("book/:book_id/related")
def book_related_books(book_id: int):
    # il y a plusieurs instances de `Book` dans le même contexte
    source_book = Book.get(book_id)

    active_books = (
        related_book
        for related_book in Book.related_books(source_book)
        if related_book.is_active
    )

    return jsonify(active_books)
```

---

## En bref

1. Construire le nom sémantique (action + sujet + produit)
1. Réduire à l'aide du contexte d'invocation
1. Qualifier les identifiants identiques

---

## Une approche automatisée?

- Les languages ne sont pas tous égaux
- Les modèles de programation ne sont pas tous égaux
- Difficile d'avoir une solution unique 
- La pluralisation est un problème
- Les exceptions de la langue sont un défi

---

# Merci!

## Des questions?
